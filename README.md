# Easy to use, multiframework - WalletConnect-TypeScript library to connect blockchain wallets to dapps.

Include Metamask browser plugin connector.

Ideal for creating dapps from connecting with any wallet. 

## Contains

- [x] [Typescript](https://www.typescriptlang.org/) 3.7.4
- [x] [WalletConnect](https://walletconnect.com/) Web3 Provider

### Build tools

- [x] [Web3 Provider](https://docs.walletconnect.com/quick-start/dapps/web3-provider)
- [x] [Ethereum Provider API](https://docs.metamask.io/guide/ethereum-provider.html)
- [x] [Metamask Onboarding](https://docs.metamask.io/guide/onboarding-library.html)
- [x] [Typescript Loader](https://github.com/TypeStrong/ts-loader)


## Installation

```
- Copy all files to your project
- Copy "devDependencies" and "dependencies" to yoru package.json file
- run npm i
```

## Usage

```
WalletAuthService[WalletEnum.metamask].connect(); //Connecting by Metamask browser plugin

WalletAuthService[WalletEnum.walletConnect].connect(); // Connecting by WalletConnect,
```
## Samples

Included usage examples:

- [x] VUE
- [x] React
- [x] Angular

## License

MIT

## Overview
![alt text](./diag.jpg)