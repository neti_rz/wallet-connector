
import MetaMaskOnboarding from '@metamask/onboarding';
import WalletAuthService from './services/wallet-auth.service';
import {WalletEnum} from './interfaces/wallet.enum';
import UserAgentService from "./services/user-agent.service";

  
  async function connectMetamask() {
    
    
      if(!MetaMaskOnboarding.isMetaMaskInstalled())
      {
        new MetaMaskOnboarding().startOnboarding();
      }
      const address = await WalletAuthService[WalletEnum.metamask].connect();
      alert('You are connected to: '+address);
      
    }
    
    if(UserAgentService.isMobile()){
        //function to hide metamask connect form mobile
      }

   async function connectWalletconect() {
         
       const address = await WalletAuthService[WalletEnum.walletConnect].connect();
       alert('You are connected to: '+address);
      
    }
      
    
  

  async function disconectWalletconnect() {
   
     
       await WalletAuthService[WalletEnum.walletConnect].disconnect();
       alert('You are disconected ');
      
    }
  
  