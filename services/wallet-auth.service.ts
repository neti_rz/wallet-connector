import {Ethereum} from "../interfaces/ethereum.interface";
import {WalletEnum} from "../interfaces/wallet.enum";
import {NetworkEnum} from "../interfaces/network.interface";
import detectEthereumProvider from '@metamask/detect-provider'
import WalletConnectProvider from "@walletconnect/web3-provider";




interface IWalletMethods {
    connect: () => Promise<string>;
    disconnect: () => Promise<void>;
    network: NetworkEnum
}

const noop: IWalletMethods = {
    connect: async (): Promise<string> => {
        return new Promise<string>(reject => {
            reject('');
        })
    },
    disconnect: async (): Promise<void> => {
        return new Promise<void>(reject => {
            reject();
        })
    },
    get network(): NetworkEnum {
        return NetworkEnum.Main
    },
    set network(network: NetworkEnum) {

    },
}

export default class WalletAuthService {

    private static walletConnectProvider?: WalletConnectProvider;

    public static readonly [WalletEnum.metamask]: IWalletMethods = {
        connect: async (): Promise<string> => {
            const provider = await detectEthereumProvider({mustBeMetaMask: true});
            if (provider) {
                const accounts = await (provider as Ethereum).request({method: 'eth_requestAccounts'});
                return accounts[0];
            } else {
                console.log('Unable to find MetaMask');
                return new Promise<string>(reject => {
                    reject('');
                })
            }
        },
        get network(): NetworkEnum {
            const Ethereum = window.ethereum as Ethereum;
            return Ethereum.networkVersion as NetworkEnum;
        },
        set network(network: NetworkEnum) {
            try {
                
                const Ethereum = window.ethereum as Ethereum;
                const chainId = `0x${network.toString(16)}`;
                Ethereum.request({
                    method: 'wallet_switchEthereumChain',
                    params: [{chainId}]
                });
            } catch (switchError) {
                
                
            }
        },
        disconnect: async(): Promise<void> => {
            
           
            
        }
    }

    public static readonly [WalletEnum.walletConnect]: IWalletMethods = {
        connect: async (): Promise<string> => {
            const provider = new WalletConnectProvider({
                infuraId: 'b7b5cca41103499999cc8277e68c081d',
                rpc: {
                    1: 'https://mainnet.infura.io/v3/07e180fc271d4e79a8a9fb1001f57a6b',
                    56: 'https://bsc-dataseed.binance.org',
                    137: 'https://rpc-mainnet.maticvigil.com',
                    42220: 'https://rpc.ankr.com/celo/'
                }
            });

            const accounts = await provider.enable();
            WalletAuthService.walletConnectProvider = provider;

            return accounts[0] || '';
        },
        disconnect: async (): Promise<void> => {
            
            
            await WalletAuthService.walletConnectProvider!.disconnect();
            WalletAuthService.walletConnectProvider = undefined;
            
        },
        get network(): NetworkEnum {
            return NetworkEnum.Main;
        },
        set network(network: NetworkEnum) {
        },
    }

    public static readonly [WalletEnum.coinbase]: IWalletMethods = noop

    public static readonly [WalletEnum.none]: IWalletMethods = noop
}
